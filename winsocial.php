<?php
if( isset($_POST) ){
     
    //var para vaçidaçao do form
    $formok = true;
    $errors = array();
     
    //Dados do post
    $ipaddress = $_SERVER['REMOTE_ADDR'];
    $date = date('d/m/Y');
    $time = date('H:i:s');
  
    
    $name = $_POST['name'];    
    $email = $_POST['email'];
    $telephone = $_POST['telephone'];
    $enquiry = $_POST['enquiry'];
    $message = $_POST['message'];
     
     
    //temos um nome?
    if(empty($name)){
        $formok = false;
        $errors[] = "Nome não foi informado";
    }
     
    //validate email address is not empty
    if(empty($email)){
        $formok = false;
        $errors[] = "Não informou e-mail";
    //validar o email
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $formok = false;
        $errors[] = "Nao entrou com endereço de email";
    }
     
    //Validar mensagem
    if(empty($message)){
        $formok = false;
        $errors[] = "Você não entrou com a mensagem";
    }
    //Validar o tamanho da mensagem
    elseif(strlen($message) < 20){
        $formok = false;
        $errors[] = "Sua mensagem tem mais de 20 caracteres";
    }
     
    //enviando o e-mail
    if($formok){
        $headers = "From: info@winsocial.com.br" . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         
        $emailbody = "<p>Recebeu um novo contato.</p>
                      <p><strong>Nome: </strong> {$name} </p>
                      <p><strong>E-mail: </strong> {$email} </p>
                      <p><strong>Telefone: </strong> {$telephone} </p>
                      <p><strong>Enquiry: </strong> {$enquiry} </p>
                      <p><strong>Mensagem: </strong> {$message} </p>
                      <p>Essa mensagem foi enviada do seguinte endereço IP: {$ipaddress} on {$date} at {$time}</p>";
         
        mail("marcos_lyra@hotmail.com","teste winsocial",$emailbody,$headers);
         
    }
     
    //montando a resposta
    $returndata = array(
        'posted_form_data' => array(
            'name' => $name,
            'email' => $email,
            'telephone' => $telephone,
            'enquiry' => $enquiry,
            'message' => $message
        ),
        'form_ok' => $formok,
        'errors' => $errors
    );
         
     
    
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest'){
        //setando variáveis de sessão
        session_start();
        $_SESSION['cf_returndata'] = $returndata;
         
        //redirecionado de volta
        header('location: ' . $_SERVER['HTTP_REFERER']);
    }
}