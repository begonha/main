<?php session_start() ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">Vc está usando um browser <strong>desatualizado</strong> . Por Favor <a href="https://browsehappy.com/">atualize seu browser</a> .</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
		<div id="contact-form" class="clearfix">
		    <h1>Tomate da Vovó!</h1>
		    <h2>Preencha seu dados abaixo e receba todas as infos sobre seu molho de tomate favorito :)</h2>
		    <?php
				//init variables
				$cf = array();
				$sr = false;
				 
				if(isset($_SESSION['cf_returndata'])){
				    $cf = $_SESSION['cf_returndata'];
				    $sr = true;
				}
			?>
		    <ul id="errors" class="<?php echo ($sr && !$cf['form_ok']) ? 'visible' : ''; ?>">
			    <li id="info">There were some problems with your form submission:</li>
    			<?php 
    				if(isset($cf['errors']) && count($cf['errors']) > 0) :
        			foreach($cf['errors'] as $error) :
    			?>
    			<li><?php echo $error ?></li>
    			<?php
        			endforeach;
    				endif;
    			?>
			</ul>
			<p id="success" class="<?php echo ($sr && $cf['form_ok']) ? 'visible' : ''; ?>">Obrigado pelo cadastro! </p>
		    <form method="post" action="winsocial.php">
		        <label for="name">Nome: <span class="required">*</span></label>
				<input type="text" id="name" name="name" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['name'] : '' ?>" placeholder="Seu Nome aqui" required="required" autofocus="autofocus" />
				 
				<label for="email">Endereço de Email : <span class="required">*</span></label>
				<input type="email" id="email" name="email" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['email'] : '' ?>" placeholder="seuNome@seuDominio.com" required="required" />
				 
				<label for="telephone">Telefone: </label>
				<input type="tel" id="telephone" name="telephone" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['telephone'] : '' ?>" />
				 
				<label for="enquiry">Enquiry: </label>
				<select id="enquiry" name="enquiry">
				    <option value="General" <?php echo ($sr && !$cf['form_ok'] && $cf['posted_form_data']['enquiry'] == 'General') ? "selected='selected'" : '' ?>>Móvel</option>
				    <option value="Support" <?php echo ($sr && !$cf['form_ok'] && $cf['posted_form_data']['enquiry'] == 'Support') ? "selected='selected'" : '' ?>>Fixo</option>
				</select>
				 
				<label for="message">Message: <span class="required">*</span></label>
				<textarea id="message" name="message" placeholder="Mensagem tem que ser mairo que 20 dígitos" required="required" data-minlength="20"><?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['message'] : '' ?></textarea>
				 
				<span id="loading"></span>
				<input type="submit" value="Enviar!" id="submit-button" />
				<p id="req-field-desc"><span class="required">*</span> indica campo necessário</p>
		    </form>
		    <?php unset($_SESSION['cf_returndata']); ?>
		</div>
    </body>
</html>
